<?php

namespace Drupal\bt_forum\Config;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * Example configuration override.
 */
class ConfigForumOverride implements ConfigFactoryOverrideInterface {

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    $overrides = array();
    if (in_array('field.field.node.forum.taxonomy_forums', $names)) {
      $overrides['field.field.node.forum.taxonomy_forums']['label'] = 'Foro';
    }
    if (in_array('field.field.node.forum.body', $names)) {
      $overrides['field.field.node.forum.body']['label'] = 'Mensaje';
    }
    if (in_array('node.type.forum', $names)) {
      $overrides['node.type.forum']['third_party_settings']['menu_ui'] = array(
        'available_menus' => array(),
        'parent' => '',
      );
    }
    if (in_array('taxonomy.vocabulary.forums', $names)) {
      $overrides['taxonomy.vocabulary.forums']['name'] = 'Foros';
      $overrides['taxonomy.vocabulary.forums']['description'] = 'Vocabulario del foro.';
    }

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'ConfigForoOverride';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}
